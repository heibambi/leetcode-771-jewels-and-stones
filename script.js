const numJewelsInStones = function(jewels, stones) {
    let numCount = 0;
    let jewelsMap = {};
    for(let jewel of jewels){
        jewelsMap[jewel] =true;
    }
    
    for(let stone of stones){
        if(jewelsMap[stone]){
            numCount++
        }
    }

    return numCount;
};

const answer1 = numJewelsInStones("aA" , "aAAbbbb");
const answer2 = numJewelsInStones("z" , "ZZ");

console.log({
    answer1,
    answer2
})